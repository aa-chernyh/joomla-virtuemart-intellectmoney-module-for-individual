<?php

defined('_JEXEC') or die('Restricted access');
define('JPATH_BASE', dirname(__FILE__));
if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

class plgVMPaymentIntellectMoney extends vmPSPlugin {

    public static $_this = false;

    function __construct(&$subject, $config) {
        parent::__construct($subject, $config);

        $this->_loggable = true;
        $this->tableFields = array_keys($this->getTableSQLFields());

        $varsToPush = array(
            'eshop_id' => array('', 'int'),
            'secret_key' => array('', 'int'),
            'account_id' => array('', 'int'),
            'form_id' => array('', 'int'),
            'form_type' => array('PeerToPeer', 'int'),
            'IntellectMoney_currency' => array(0, 'int'),
            'payment_logos' => array('../../../../plugins/vmpayment/intellectmoney/im_logo.png', 'char'),
            'countries' => array(0, 'char'),
            'min_amount' => array(0, 'int'),
            'max_amount' => array(0, 'int'),
            'cost_per_transaction' => array(0, 'int'),
            'cost_percent_total' => array(0, 'int'),
            'tax_id' => array(0, 'int')
        );

        $this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
    }

    protected function getVmPluginCreateTableSQL() {
        return $this->createTableSQL('Payment IntellectMoney Table');
    }

    function getTableSQLFields() {

        $SQLfields = array(
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'virtuemart_order_id' => 'int(1) UNSIGNED',
            'order_number' => 'char(64)',
            'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED',
            'payment_name' => 'varchar(5000)',
            'payment_order_total' => 'decimal(15,5) NOT NULL',
            'payment_currency' => 'smallint(1)',
            'email_currency' => 'smallint(1)',
            'cost_per_transaction' => 'decimal(10,2)',
            'cost_percent_total' => 'decimal(10,2)',
            'tax_id' => 'smallint(1)',
            'IntellectMoney_custom' => 'varchar(255)',
            'IntellectMoney_response_mc_gross' => 'decimal(10,2)',
            'IntellectMoney_response_mc_currency' => 'char(10)',
            'IntellectMoney_response_invoice' => 'char(32)',
            'IntellectMoney_response_protection_eligibility' => 'char(128)',
            'IntellectMoney_response_payer_id' => 'char(13)',
            'IntellectMoney_response_tax' => 'decimal(10,2)',
            'IntellectMoney_response_payment_date' => 'char(28)',
            'IntellectMoney_response_payment_status' => 'char(50)',
            'IntellectMoney_response_mc_fee' => 'decimal(10,2)',
            'IntellectMoney_response_payer_email' => 'char(128)',
            'IntellectMoney_response_last_name' => 'char(64)',
            'IntellectMoney_response_first_name' => 'char(64)',
            'IntellectMoney_response_business' => 'char(128)',
            'IntellectMoney_response_receiver_email' => 'char(128)',
            'IntellectMoney_response_transaction_subject' => 'char(128)',
            'IntellectMoney_response_residence_country' => 'char(2)',
            'IntellectMoneyresponse_raw' => 'varchar(512)'
        );
        return $SQLfields;
    }

    function plgVmConfirmedOrder($cart, $order) {

        if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
            return null;
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        $session = JFactory::getSession();
        $return_context = $session->getId();
        $this->_debug = $method->debug;
        $this->logInfo('plgVmConfirmedOrder order number: ' . $order['details']['BT']->order_number, 'message');

        if (!class_exists('VirtueMartModelOrders'))
            require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
        if (!class_exists('VirtueMartModelCurrency'))
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'currency.php');

        $new_status = '';

        $usrBT = $order['details']['BT'];
        $address = ((isset($order['details']['ST'])) ? $order['details']['ST'] : $order['details']['BT']);

        $vendorModel = new VirtueMartModelVendor();
        $vendorModel->setId(1);
        $vendor = $vendorModel->getVendor();
        $this->getPaymentCurrency($method);
        $q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $method->payment_currency . '" ';
        $db = &JFactory::getDBO();
        $db->setQuery($q);
        $currency_code_3 = $db->loadResult();

        $paymentCurrency = CurrencyDisplay::getInstance($method->payment_currency);
        $totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $order['details']['BT']->order_total, false), 2);
        $cd = CurrencyDisplay::getInstance($cart->pricesCurrency);



        $eshop_id = $this->_geteshop_id($method);
        $orderId = $this->_getaccid($method);
        $account_id = $this->_get_account_id($method);
        $form_id = $this->_get_form_id($method);
        $form_type = $this->_get_form_type($method);
        $recipientCurrency = "RUB";
        $josvirtuemart_order_id = $order['details']['BT']->virtuemart_order_id;
        $domain = str_replace("www.", "", $_SERVER['SERVER_NAME']);
        $order_id = $order['details']['BT']->order_number;
        $user_e_mail = $address->email;
        $service_name = $form_type == 'IMAccount' ? 'Перевод на кошелёк' : 'Перевод на карту';
        $control_hash = $eshop_id . "::" . $order_id . "::" . $serviceName . "::" . $totalInPaymentCurrency . "::" . $recipientCurrency . '::' . $secretKey;

        $hash = md5($control_hash);
        $post_variables = Array(
            "userName" => $address->first_name,
            "eshopId" => $eshop_id,
            "orderId" => $order['details']['BT']->order_number,
            "recipientAmount" => $totalInPaymentCurrency,
            "recipientCurrency" => $recipientCurrency,
            "userEmail" => $address->email,
            "FormType" => $form_type,
            'UserField_1' => $return_context,
            'UserField_0' => $account_id,
            'UserFieldName_0' => $service_name,
            'UserField_2' => $order_id,
            //'UserFieldName_2' => "Номер заказа",
            //'UserField_9' => $josvirtuemart_order_id,
            'UserFieldName_9' => "UserPaymentFormId",
            "hash" => $hash,
            "successUrl" => JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived&pm=' . $order['details']['BT']->virtuemart_paymentmethod_id),
            "failUrl" => JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginUserPaymentCancel&on=' . $order['details']['BT']->order_number . '&pm=' . $order['details']['BT']->virtuemart_paymentmethod_id)
        );
        $url = 'https://merchant.intellectmoney.ru/ru/';


        $html = '<form action="' . "" . $url . '" method="post" name="vm_intellekt_money_form" >';
        $html.= '<input type="image" name="submit" src="https://www.intellectmoney.ru/common/img/uploaded/banners/8ec63f/120x60_1.gif" alt="Intellectmoney.ru" />';
        foreach ($post_variables as $name => $value) {
            $html.= '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars($value) . '" />';
        }
        $html.= '</form>';
        $html.= ' <script type="text/javascript">';
        $html.= ' document.vm_intellekt_money_form.submit();';
        $html.= ' </script>';

        $cart->emptyCart();
        JRequest::setVar('html', $html);
    }

    function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return null;
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        $this->getPaymentCurrency($method);
        $paymentCurrencyId = $method->payment_currency;
    }

    function plgVmOnPaymentResponseReceived(&$html) {

        $virtuemart_paymentmethod_id = JRequest::getInt('pm', 0);

        $vendorId = 0;
        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return null;
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        $payment_data = JRequest::get('post');
        vmdebug('plgVmOnPaymentResponseReceived', $payment_data);
        $order_number = $payment_data['orderId'];
        $return_context = $payment_data['UserField_1'];
        if (!class_exists('VirtueMartModelOrders'))
            require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

        $virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number);
        $payment_name = $this->renderPluginName($method);


        if ($virtuemart_order_id) {
            if (!class_exists('VirtueMartCart'))
                require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');

            $cart = VirtueMartCart::getCart();

            if (!class_exists('VirtueMartModelOrders'))
                require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
            $order = new VirtueMartModelOrders();
            $orderitems = $order->getOrder($virtuemart_order_id);

            $cart->sentOrderConfirmedEmail($orderitems);

            $cart->emptyCart();
        }

        return true;
    }

    function plgVmOnUserPaymentCancel() {

        if (!class_exists('VirtueMartModelOrders'))
            require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

        $order_number = JRequest::getVar('on');
        if (!$order_number)
            return false;
        $db = JFactory::getDBO();
        $query = 'SELECT ' . $this->_tablename . '.`virtuemart_order_id` FROM ' . $this->_tablename . " WHERE  `order_number`= '" . $order_number . "'";

        $db->setQuery($query);
        $virtuemart_order_id = $db->loadResult();

        if (!$virtuemart_order_id) {
            return null;
        }
        $this->handlePaymentUserCancel($virtuemart_order_id);

        return true;
    }

    function plgVmOnPaymentNotification() {

        if (!class_exists('VirtueMartModelOrders'))
            require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
        $paypal_data = JRequest::get('post');

        $order_number = $paypal_data['orderId'];
        $virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($paypal_data['orderId']);
        $this->logInfo('plgVmOnPaymentNotification: virtuemart_order_id  found ' . $virtuemart_order_id, 'message');

        if (!$virtuemart_order_id) {
            $this->_debug = true;
            $this->logInfo('plgVmOnPaymentNotification: virtuemart_order_id not found ', 'ERROR');
            $this->sendEmailToVendorAndAdmins(JText::_('VMPAYMENT_PAYPAL_ERROR_EMAIL_SUBJECT'), JText::_('VMPAYMENT_PAYPAL_UNKNOW_ORDER_ID'));
            exit;
        }
        $vendorId = 0;
        $payment = $this->getDataByOrderId($virtuemart_order_id);

        $method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        $this->_debug = $method->debug;
        if (!$payment) {
            $this->logInfo('getDataByOrderId payment not found: exit ', 'ERROR');
            return null;
        }
        $this->logInfo('paypal_data ' . implode('   ', $paypal_data), 'message');

        $db = JFactory::getDBO();
        $query = 'SHOW COLUMNS FROM `' . $this->_tablename . '` ';
        $db->setQuery($query);
        $columns = $db->loadResultArray(0);
        $post_msg = '';
        foreach ($paypal_data as $key => $value) {
            $post_msg .= $key . "=" . $value . "<br />";
            $table_key = 'paypal_response_' . $key;
            if (in_array($table_key, $columns)) {
                $response_fields[$table_key] = $value;
            }
        }
        $response_fields['payment_name'] = $this->renderPluginName($method);
        $response_fields['paypalresponse_raw'] = $post_msg;
        $return_context = $paypal_data['UserField_1'];
        $response_fields['order_number'] = $order_number;
        $response_fields['virtuemart_order_id'] = $virtuemart_order_id;

        $this->storePSPluginInternalData($response_fields);

        $error_msg = $this->_processIPN($paypal_data, $method);
        $this->logInfo('process IPN ' . $error_msg, 'message');
        if (!(empty($error_msg) )) {
            $new_status = $method->status_canceled;
            $this->logInfo('process IPN ' . $error_msg . ' ' . $new_status, 'ERROR');
        } else {
            $this->logInfo('process IPN OK, status', 'message');


            if (empty($paypal_data['payment_status']) || ($paypal_data['payment_status'] != 'Completed' && $paypal_data['payment_status'] != 'Pending')) {
            }
            $paypal_status = $paypal_data['payment_status'];
            if (strcmp($paypal_status, 'Completed') == 0) {
                $new_status = $method->status_success;
            }
        }




        if ($virtuemart_order_id) {
            if (!class_exists('VirtueMartModelOrders'))
                require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
            $modelOrder = new VirtueMartModelOrders();
            $order['order_status'] = $new_status;
            $order['virtuemart_order_id'] = $virtuemart_order_id;
            $order['customer_notified'] = 1;
            $order['comments'] = JTExt::sprintf('VMPAYMENT_PAYPAL_PAYMENT_CONFIRMED', $order_number);
            $modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
        }
        $this->emptyCart($return_context);
        return true;
    }

    function _geteshop_id($method) {
        return $method->eshop_id;
    }

    function _getaccid($method) {
        return $method->accaunt_id;
    }

    function _getskey($method) {
        return $method->secret_key;
    }

    function _debug($method) {
        return $method->debug;
    }

    function _get_account_id($method) {
        return $method->account_id;
    }

    function _get_form_id($method) {
        return $method->form_id;
    }
    
    function _get_form_type($method) {
        return $method->form_type;
    }

    function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
        if (preg_match('/%$/', $method->cost_percent_total)) {
            $cost_percent_total = substr($method->cost_percent_total, 0, -1);
        } else {
            $cost_percent_total = $method->cost_percent_total;
        }
        return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
    }

    protected function checkConditions($cart, $method, $cart_prices) {


        $address = (($cart->ST == 0) ? $cart->BT : $cart->ST);

        $amount = $cart_prices['salesPrice'];
        $amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount
                OR ( $method->min_amount <= $amount AND ( $method->max_amount == 0) ));

        $countries = array();
        if (!empty($method->countries)) {
            if (!is_array($method->countries)) {
                $countries[0] = $method->countries;
            } else {
                $countries = $method->countries;
            }
        }

        if (!is_array($address)) {
            $address = array();
            $address['virtuemart_country_id'] = 0;
        }

        if (!isset($address['virtuemart_country_id']))
            $address['virtuemart_country_id'] = 0;
        if (in_array($address['virtuemart_country_id'], $countries) || count($countries) == 0) {
            if ($amount_cond) {
                return true;
            }
        }

        return false;
    }

    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {

        return $this->onStoreInstallPluginTable($jplugin_id);
    }

    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
        return $this->OnSelectCheck($cart);
    }

    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
        return $this->displayListFE($cart, $selected, $htmlIn);
    }

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
        return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
        return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }

    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
        $this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

    function plgVmonShowOrderPrintPayment($order_number, $method_id) {
        return $this->onShowOrderPrint($order_number, $method_id);
    }

    function plgVmDeclarePluginParamsPayment($name, $id, &$data) {
        return $this->declarePluginParams('payment', $name, $id, $data);
    }

    function plgVmDeclarePluginParamsPaymentVM3(&$data) {
        return $this->declarePluginParams('payment', $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
        return $this->setOnTablePluginParams($name, $id, $table);
    }

}