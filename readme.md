#Модуль оплаты платежной системы IntellectMoney для CMS Joomla с модулем VirtueMart для физических лиц

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557756#whatnew.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=557756#5577569eb3d78256714a6193e51c807868ea9e